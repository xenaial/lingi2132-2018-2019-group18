.. _part6:

*************************************************************************************************
Partie 6 | DSL and Scala
*************************************************************************************************

Question proposed by Group 05, Desausoi Laurent & Dhillon Sundeep
=================================================================

1. Explain the main differences between an object and a class in Scala.
-----------------------------------------------------------------------
2. Explain the difference between var and val. Is there one that we should prefereably use ? What could be their equivalent in Java?
------------------------------------------------------------------------------------------------------------------------------------
3. Define what currying is.
---------------------------
3.1. Apply the currying technique of the following uncurried function.
----------------------------------------------------------------------
.. code-block:: scala

  def minus(x: Int, y: Int): Int = {
    x - y
  }
4. Convert the following Java code (in which we simply multiply each element of 2 matrixes) into Scala code.
------------------------------------------------------------------------------------------------------------
.. code-block:: java

  public static int[][] matrixMultiply(int[][] a, int[][] b) {
    int[][] c = new int[a.length][a[0].length];

    for (int i = 0; i < a.length; i++)
        for (int j = 0; j < a[i].length; j++)
            c[i][j] = a[i][j] * b[i][j];
    return c;
  }


""""""""""""""

Answers
=======

1. An object is an instance of a class which is guaranteed to be unique at a certain time T (it follows the Singleton pattern). Once an object is created,
a class is more a definition, a description. It defines a type in terms of methods and composition of other types. In Scala, we create objects instead of classes.

2. Var is mutable (we can reassign its value) unlike val which is unmutable (but we can still change the object's state, its internal variable).
In Scala, we prefer to use val instead of var because it increases the readability.
In Java, the equivalent would be the following: val is a final variable and var just a "normal" variable.

3. It's a special technique thanks to which we can transform a function with several arguments and translate it into a sequence of functions, each with a single argument.
You can try turn any function with multiple arguments intos its curried equivalent.

3.1.

.. code-block:: scala

  def minus(x: Int): (Int => Int) = {
    (y: Int) => {
      x - y
    }
  }

Or equivalently:

.. code-block:: scala

  def minus(x: Int)(y: Int): Int = {
    x - y
  }

4.

.. code-block:: scala

  def matrixMultiply(a: Array[Array[Int]], b: Array[Array[Int]]): Array[Array[Int]] = {
    val c: Array[Array[Int]] = Array.ofDim[Int](a.length, a(0).length)
    for(i <- 0 until a.length; j <- 0 until a(i).length) {
      c(i)(j) = a(i)(j) * b(i)(j)
    }
    c
  }

""""""""""""""

Understanding traits
====================
**by Group 18: Bénéré Maxime and Xenakis Alexandros** 

Questions
---------

#. Compare traits and interfaces. What are the advantages of using each one? Should they be used in a mutually exclusive way? 
#. Compare traits and abstract classes. What are the advantages of using each one? Should they be used in a mutually exclusive way?
#. If a class extends multiple traits, is the order in which it extends them important? Illustrate with an example.


Answers
-------

1. We could say that traits are richer version of interfaces, as traits can contain implementation. Indeed, traits are extended by classes, whereas interfaces are implemented by classes. This can be a big advange, as it allows to build richer interfaces. If we have an interface with many methods, all classes implementing the interface have to implement each method, even if some of the methods are not very relevant to the class. This can cause code duplication, as well as less readable code. On the other hand, if a class extends a trait, the implemented methods of the trait will be passed on to the child, who does not need to implement them again. It can override them if needed in order to alter some functionality. This being said, the two can be used together, they are not mutually exclusive. For example having an iterface implemented by a trait, which can then be extended.

2. Both traits and abstract classes let the user define a type with a given behavior, that can be altered by classes that extend them. There are two major differences between the two however. First, a class can extend multiple traits. This is not the case with abstract classes. This is usefull for creating a more elaborate inheritance structure. However, this can also make the code more complicated. Another difference is that the traits don't have constructor parameters, whereas abstract classes do. A possible solution to get the best of the two can be to combine them by having a trait extend an abstract class in order to benefit from its constructor parameters.

3. The order in which a class extends traits can be important, but is not always. It depends on whether the traits modify the state of the class. The important point, however, is that scala puts all traits in a linear inheritance hierarchy. This process is called linearization and determines how the program is executed. To illustrate, let's look at the following example. 
    
    .. code-block:: scala
        
        class Operation {
          var myInt = 4 
          def apply = println(myInt)
        }
    
        trait PlusOperation extends Operation {
          override def apply = {
            myInt += 2
            super.apply
          } 
        }
    
        trait TimesOperation extends Operation {
          override def apply = {
            myInt = myInt*2
            super.apply
          }
        }
    
        class Calculation1 extends PlusOperation with TimesOperation {} 
        class Calculation2 extends TimesOperation with PlusOperation {}
    
        object Test extends App {
    
          val c1 = new Calculation1
          print("Calculation 1 gives: ")
          c1.apply 
    
          val c2 = new Calculation2
          print("Calculation 2 gives: ")
          c2.apply
        }
        
   If we execute it, we see that Calculation1 prints 10 whereas Calculation2 prints 12. This happends as Scala first evaluates the rightmost trait and then continues with the traits towards the left. Therefore, Calculation1 will evaluate 4*2+2 = 10, when Calculation2 will evaluate (4+2)*2 = 12.
